package com.test.alex.testtask;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.transition.Slide;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.view.View;
import android.widget.ImageView;

import com.rey.material.widget.LinearLayout;
import com.squareup.picasso.Picasso;

public class MainActivity extends AppCompatActivity {

    protected Toolbar toolbar;
    protected ImageView mImageViewWeather;
    protected LinearLayout mLinearLayoutQuiz;
    protected LinearLayout mLinearLayoutWeather;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle("Погода");
        setSupportActionBar(toolbar);
        mImageViewWeather =(ImageView)findViewById(R.id.iw_weather);
        mLinearLayoutQuiz = (LinearLayout)findViewById(R.id.ll_quiz);
        mLinearLayoutWeather = (LinearLayout)findViewById(R.id.ll_weather);
        Picasso.with(this).load(R.drawable.weather).into(mImageViewWeather);
        mLinearLayoutQuiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, QuizActivity.class));
            }
        });
        mLinearLayoutWeather.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, NewsActivity.class));
            }
        });


    }


}
