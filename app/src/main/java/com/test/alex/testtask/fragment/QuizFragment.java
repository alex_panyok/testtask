package com.test.alex.testtask.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.rey.material.widget.RadioButton;
import com.test.alex.testtask.R;
import com.test.alex.testtask.TestTaskKeys;

/**
 * Created by Alex on 19.01.2016.
 */
public class QuizFragment extends Fragment implements TestTaskKeys {

    protected RadioButton mRadioButtonYes;

    protected RadioButton mRadioButtonNo;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_quiz,container,false);
        mRadioButtonYes = (RadioButton)view.findViewById(R.id.rb_yes);
        mRadioButtonNo = (RadioButton)view.findViewById(R.id.rb_no);
        mRadioButtonYes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                getActivity().getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE).edit().putFloat("YES",(getActivity().getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE).getFloat("YES",0)+1)).apply();
                getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in, R.anim.slide_out, R.anim.push_slide_in, R.anim.pop_slide_out).replace(R.id.container, new QuizResultFragment()).commit();
            }
        });

        mRadioButtonNo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                getActivity().getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE).edit().putFloat("NO",(getActivity().getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE).getFloat("NO",0)+1)).apply();
                getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in, R.anim.slide_out, R.anim.push_slide_in, R.anim.pop_slide_out).replace(R.id.container, new QuizResultFragment()).commit();
            }
        });
        return view;
    }
}
