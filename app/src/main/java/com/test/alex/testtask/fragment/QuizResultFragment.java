package com.test.alex.testtask.fragment;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.test.alex.testtask.R;
import com.test.alex.testtask.TestTaskKeys;

import java.util.ArrayList;

/**
 * Created by Alex on 19.01.2016.
 */
public class QuizResultFragment extends Fragment implements TestTaskKeys {

    protected PieChart pieChart;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_res_quiz, container,false);
        pieChart = (PieChart)view.findViewById(R.id.chart);
        setUpChart();
        return view;
    }

    void setUpChart(){
        pieChart.animateY(1400, Easing.EasingOption.EaseInOutQuad);
        pieChart.setUsePercentValues(true);
        pieChart.setDescription("");
        pieChart.getLegend().setEnabled(false);
        ArrayList<Integer> colors = new ArrayList<Integer>();
        colors.add(Color.GREEN);
        colors.add(Color.RED);
        ArrayList<Entry> yVals1 = new ArrayList<Entry>();
        yVals1.add(new Entry( getActivity().getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE).getFloat("YES",0),0));
        yVals1.add(new Entry(getActivity().getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE).getFloat("NO",0),1));
        ArrayList<String> xVals = new ArrayList<String>();
        xVals.add("ДА");
        xVals.add("НЕТ");
        PieDataSet dataSet = new PieDataSet(yVals1, "Результат опроса");
        dataSet.setColors(colors);
        PieData data = new PieData(xVals, dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(15f);
        pieChart.setData(data);

    }
}
